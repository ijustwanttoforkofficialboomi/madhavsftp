/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp;

import com.jcraft.jsch.Session;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class DHGEX256_1024 extends DHGEX1024 {
	public void init(Session session, byte[] V_S, byte[] V_C, byte[] I_S, byte[] I_C) throws Exception {
		this.hash = "sha-256";
		super.init(session, V_S, V_C, I_S, I_C);
	}
}
