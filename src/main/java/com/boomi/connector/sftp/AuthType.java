/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public enum AuthType {

	PASSWORD("Username and Password"), PUBLIC_KEY("Using public Key");

	private String authTypeDesc;

	AuthType(String p) {
		authTypeDesc = p;
	}

	public String getAuthType() {
		return authTypeDesc;
	}
}
