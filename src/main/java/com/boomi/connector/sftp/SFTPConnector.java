/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.sftp.operations.SFTPCreateOperation;
import com.boomi.connector.sftp.operations.SFTPDeleteOperation;
import com.boomi.connector.sftp.operations.SFTPGetOperation;
import com.boomi.connector.sftp.operations.SFTPQueryOperation;
import com.boomi.connector.sftp.operations.SFTPUpsertOperation;
import com.boomi.connector.util.BaseConnector;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPConnector extends BaseConnector {

	@Override
	public Browser createBrowser(BrowseContext context) {
		return new SFTPBrowser(createConnection(context));
	}

	private SFTPConnection createConnection(BrowseContext context) {
		return new SFTPConnection(context);
	}

	@Override
	protected Operation createCreateOperation(OperationContext context) {
		return new SFTPCreateOperation(createConnection(context));
	}

	@Override
	protected Operation createGetOperation(OperationContext context) {
		return new SFTPGetOperation(createConnection(context));
	}

	@Override
	protected Operation createQueryOperation(OperationContext context) {
		return new SFTPQueryOperation(createConnection(context));
	}

	@Override
	protected Operation createDeleteOperation(OperationContext context) {
		return new SFTPDeleteOperation(createConnection(context));
	}
	
	@Override
    protected Operation createUpsertOperation(OperationContext context) {
        return new SFTPUpsertOperation(createConnection(context));
    }
}
