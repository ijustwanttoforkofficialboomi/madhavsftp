/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.retry;

import com.boomi.util.retry.PhasedRetry;
import com.boomi.util.retry.RetryStrategy;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
final class LimitedRetryStrategyFactory extends RetryStrategyFactory {
	private final int maxRetries;

	LimitedRetryStrategyFactory(int maxRetries) {
		this.maxRetries = maxRetries;
	}

	@Override
	public RetryStrategy createRetryStrategy() {
		return new PhasedRetry(this.maxRetries);
	}
}
