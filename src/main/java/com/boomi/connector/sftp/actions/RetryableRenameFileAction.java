/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableRenameFileAction extends SingleRetryAction {
	private final String originalPath;
	private final String newPath;

	public RetryableRenameFileAction(SFTPConnection connection, String originalPath, String newPath) {
		super(connection, null, null);
		this.originalPath = originalPath;
		this.newPath = newPath;
	}

	@Override
	public void doExecute() {
		this.getConnection().renameFile(this.originalPath, this.newPath);
	}
}
