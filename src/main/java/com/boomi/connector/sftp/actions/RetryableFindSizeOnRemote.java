/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableFindSizeOnRemote extends SingleRetryAction {

	private Long fileSize;

	private String enteredFileName;
	private String remoteDir;

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getEnteredFileName() {
		return enteredFileName;
	}

	public void setEnteredFileName(String enteredFileName) {
		this.enteredFileName = enteredFileName;
	}

	public String getRemoteDir() {
		return remoteDir;
	}

	public void setRemoteDir(String remoteDir) {
		this.remoteDir = remoteDir;
	}

	public RetryableFindSizeOnRemote(SFTPConnection connection, String remoteDir, String fileName, TrackedData input) {
		super(connection, remoteDir, input);

	}

	public RetryableFindSizeOnRemote(SFTPConnection connection, String remoteDir,String enteredFileName) {
		super(connection, null, null);
		this.remoteDir = remoteDir;
		this.enteredFileName = enteredFileName;

	}

	@Override
	public void doExecute() {
		fileSize = this.getConnection().getSizeOnRemote(remoteDir, enteredFileName);
	}

}
