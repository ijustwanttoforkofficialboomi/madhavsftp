/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableVerifyFileExistsAction extends SingleRetryAction {
	private String fullPath;
	private Boolean fileExists;

	public Boolean getFileExists() {
		return fileExists;
	}

	public void setFileExists(Boolean fileExists) {
		this.fileExists = fileExists;
	}

	public RetryableVerifyFileExistsAction(SFTPConnection connection, String remoteDir, String fileName,
			TrackedData input) {
		super(connection, remoteDir, input);

	}

	public RetryableVerifyFileExistsAction(SFTPConnection connection, String fullPath) {
		super(connection, null, null);
		this.fullPath = fullPath;
	}

	public RetryableVerifyFileExistsAction(SFTPConnection connection, String remoteDir, String fileName) {
		super(connection, null, null);
		this.fullPath = connection.getPathsHandler().joinPaths(remoteDir, fileName);
	}

	@Override
	public void doExecute() {
		fileExists = this.getConnection().fileExists(fullPath);
	}

}
