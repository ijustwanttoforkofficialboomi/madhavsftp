/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import java.util.List;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;
import com.jcraft.jsch.ChannelSftp.LsEntry;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableGetFileListInDirectory extends RetryableAction {
	private String fileName;
	private List<LsEntry> fileList;

	public List<LsEntry> get_fileList() {
		return fileList;
	}

	public void set_fileList(List<LsEntry> _fileList) {
		this.fileList = _fileList;
	}

	public void set_fileName(String _fileName) {
		this.fileName = _fileName;
	}

	public String get_fileName() {
		return fileName;
	}

	public RetryableGetFileListInDirectory(SFTPConnection connection, String remoteDir, TrackedData input,
			String fileName, RetryStrategyFactory retryFactory) {
		super(connection, remoteDir, input, retryFactory);
		this.fileName = fileName;
	}

	public RetryableGetFileListInDirectory(SFTPConnection connection, String remoteDir, TrackedData input,
			RetryStrategyFactory retryFactory) {
		super(connection, remoteDir, input, retryFactory);

	}

	@Override
	public void doExecute() {
		this.fileList = this.getConnection().getFileListInDirectory(super.getRemoteDir());
	}
}
