/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp;


import com.jcraft.jsch.DHGEX;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class DHGEX1024
extends DHGEX {
    private static final int MAX_KEYSIZE = 1024;

    protected int check2048(Class c, int _max) throws Exception {
        return _max > MAX_KEYSIZE ? MAX_KEYSIZE : _max;
    }
}

