/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.exception;

 /**
  * @author Omesh Deoli
  *
  * ${tags}
  */
public class NoSuchFileFoundException extends RuntimeException {

	private static final long serialVersionUID = -6908750692873284245L;

	public NoSuchFileFoundException(String statusMessage) {
		super(statusMessage);
		
	}

	public NoSuchFileFoundException(String errMessage, Throwable e) {
		super(errMessage, e);
	}

}
