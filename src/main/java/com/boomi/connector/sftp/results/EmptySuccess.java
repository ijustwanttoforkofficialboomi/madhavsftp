/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.results;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.TrackedData;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class EmptySuccess
implements Result {
    @Override
    public void addToResponse(OperationResponse response, TrackedData input) {
        ResponseUtil.addEmptySuccess((OperationResponse)response, (TrackedData)input, (String)"0");
    }
}

