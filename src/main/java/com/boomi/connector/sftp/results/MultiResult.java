/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.results;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.util.IOUtil;
import com.boomi.util.LogUtil;
import com.boomi.util.TempOutputStream;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class MultiResult {

	private final OperationResponse response;
	private final FilterData input;
	private int size;
	private int errorCount = 0;
	private TempOutputStream errDoc;
	private JsonGenerator generator;

	public MultiResult(OperationResponse response, FilterData input) {
		this.response = response;
		this.input = input;
	}

	private void initErrorDoc() {
		if (this.errDoc == null) {
			this.errDoc = new TempOutputStream();
			try {
				this.generator = new ObjectMapper().getFactory().createGenerator((OutputStream) this.errDoc);
				this.generator.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
				this.generator.enable(JsonGenerator.Feature.AUTO_CLOSE_JSON_CONTENT);
				this.generator.enable(JsonGenerator.Feature.ESCAPE_NON_ASCII);
				this.generator.writeStartObject();
				this.generator.writeArrayFieldStart(SFTPConstants.ERROR_LIST);
			} catch (IOException e) {
				IOUtil.closeQuietly((Closeable[]) new Closeable[] { this.generator, this.errDoc });
				this.errDoc = null;
				this.generator = null;
				throw new ConnectorException((Throwable) e);
			}
		}
	}

	public void addPartialResult(String fileName, BaseResult result) {
		if (result.getStatus() == OperationStatus.APPLICATION_ERROR) {
			this.initErrorDoc();
			try {
				this.writeErrorResult(fileName, result);
			} catch (IOException e) {
				throw new ConnectorException((Throwable) e);
			}
		} else {
			this.addPartialResult(result);
		}
	}

	private void writeErrorResult(String fileName, BaseResult result) throws IOException {
		this.generator.writeStartObject();
		try {
			this.generator.writeStringField(SFTPConstants.PROPERTY_FILENAME, fileName);
			this.generator.writeStringField(SFTPConstants.MESSAGE, result.getStatusMessage());
			this.generator.writeNumberField(SFTPConstants.CODE, Integer.parseInt(result.getStatusCode()));
		} finally {
			++this.errorCount;
			this.generator.writeEndObject();
		}
	}

	public void addPartialResult(BaseResult result) {
		this.addPartial(result);
		++this.size;
	}

	private void addPartial(BaseResult result) {
		this.response.addPartialResult((TrackedData) this.input, result.getStatus(), result.getStatusCode(),
				result.getStatusMessage(), result.getPayload());
	}

	public void finish() {
		try {
			this.finishErrDoc();
		} finally {
			this.response.finishPartialResult((TrackedData) this.input);
		}
	}

	private void finishErrDoc() {
		if (this.errDoc != null) {
			InputStream inp = null;
			try {
				this.generator.flush();
				this.generator.close();
				inp = this.errDoc.toInputStream();
				BaseResult errResult = new BaseResult(PayloadUtil.toPayload(inp), "-1",
						String.format(SFTPConstants.ERROR_OCCURED_IN_FILES, this.errorCount),
						OperationStatus.APPLICATION_ERROR);
				this.addPartial(errResult);
			} catch (Exception e) {
				try {
					LogUtil.warning((Logger) this.input.getLogger(), (Throwable) e, SFTPConstants.ERROR_IN_ERR_DOC,
							(Object[]) new Object[0]);
					this.addPartial(new ErrorResult(OperationStatus.APPLICATION_ERROR, e));
				} catch (Exception ex) {
					IOUtil.closeQuietly((Closeable[]) new Closeable[] { inp, this.generator, this.errDoc });
				}
			} finally {
				IOUtil.closeQuietly((Closeable[]) new Closeable[] { inp, this.generator, this.errDoc });
			}
		}
	}

	public FilterData getInput() {
		return this.input;
	}

	public int getSize() {
		return this.size;
	}

	public boolean isEmpty() {
		return this.size == 0;
	}
}
