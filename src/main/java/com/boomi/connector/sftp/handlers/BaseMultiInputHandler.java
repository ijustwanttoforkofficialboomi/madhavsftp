/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.handlers;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.actions.RetryableChangeDirectoryAction;
import com.boomi.connector.sftp.actions.RetryableVerifyFileExistsAction;
import com.boomi.connector.sftp.common.PathsHandler;
import com.boomi.connector.sftp.common.SFTPFileMetadata;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.connector.sftp.results.ErrorResult;
import com.boomi.util.ObjectUtil;
import java.util.logging.Level;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
abstract class BaseMultiInputHandler<T extends TrackedData> {
	final SFTPConnection connection;
	final OperationResponse operationResponse;
	final String defaultWorkingDirFullPath;
	final PathsHandler pathsHandler;

	BaseMultiInputHandler(SFTPConnection connection, OperationResponse operationResponse) {
		this.connection = connection;
		this.operationResponse = operationResponse;
		this.defaultWorkingDirFullPath = this.connection.getHomeDirectory();
		this.pathsHandler = this.connection.getPathsHandler();

	}

	public void processMultiInput(UpdateRequest updateRequest) {
		for (ObjectData input : updateRequest) {
			try {
				this.processInput((T) input);
			} catch (Exception e) {
				this.addApplicationErrorResult((T) input, e);
			}
		}
	}

	abstract void processInput(T var1);

	private void addApplicationErrorResult(T input, Exception e) {
		input.getLogger().log(Level.WARNING, e.getMessage(), e);
		String statusCode = ErrorResult.inferCode(e);
		OperationStatus status = ErrorResult.inferStatus(e);
		this.operationResponse.addResult(input, status, statusCode, e.getMessage(), null);
	}

	String toFullPath(String childPath) {
		return this.pathsHandler.resolvePaths(this.defaultWorkingDirFullPath, childPath);
	}

	SFTPFileMetadata extractRemoteDirAndFileName(ObjectIdData input) {
		return this.extractRemoteDirAndFileName((TrackedData) input, input.getObjectId());
	}

	SFTPFileMetadata extractRemoteDirAndFileName(TrackedData input, String filePath) {
		String remoteDir = this.toFullPath(this.connection.getEnteredRemoteDirectory(input));

		RetryableVerifyFileExistsAction fileExistsAction = new RetryableVerifyFileExistsAction(connection, remoteDir);
		fileExistsAction.execute();
		boolean fileExists = fileExistsAction.getFileExists();

		SFTPFileMetadata fileMetadata = this.pathsHandler.splitIntoDirAndFileName(remoteDir, filePath);
		remoteDir = fileMetadata.getDirectory();
		if (!ObjectUtil.equals((Object) remoteDir, (Object) this.defaultWorkingDirFullPath) && fileExists) {
			RetryableChangeDirectoryAction changeDirAcion = new RetryableChangeDirectoryAction(connection, remoteDir);
			changeDirAcion.execute();			
		}
		if (!fileExists)
			throw new SFTPSdkException(SFTPConstants.ERROR_REMOTE_DIRECTORY_NOT_FOUND);

		return fileMetadata;
	}
}
