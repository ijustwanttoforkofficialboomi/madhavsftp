/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.handlers;

import java.io.IOException;
import java.util.UUID;

import com.boomi.util.ObjectUtil;
import com.boomi.util.StringUtil;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.SFTPUtil;
import com.boomi.connector.sftp.actions.RetryableChangeDirectoryAction;
import com.boomi.connector.sftp.actions.RetryableCreateDirectoryAction;
import com.boomi.connector.sftp.actions.RetryableDeleteFileAtPathAction;
import com.boomi.connector.sftp.actions.RetryableFindSizeOnRemote;
import com.boomi.connector.sftp.actions.RetryableRenameFileAction;
import com.boomi.connector.sftp.actions.RetryableUploadFileAction;
import com.boomi.connector.sftp.actions.RetryableVerifyFileExistsAction;
import com.boomi.connector.sftp.common.ExtendedSFTPFileMetadata;
import com.boomi.connector.sftp.common.SFTPFileMetadata;
import com.boomi.connector.sftp.common.SimpleSFTPFileMetadata;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class UpsertHandler extends BaseMultiInputHandler<ObjectData> {

	private final boolean includeAllMetadata;

	private final boolean isAppendEnabled;
	private final boolean createDir;
	private final RetryStrategyFactory uploadRetryFactory;

	public UpsertHandler(SFTPConnection connection, OperationResponse operationResponse) throws IOException {
		super(connection, operationResponse);
		PropertyMap opProperties = this.connection.getContext().getOperationProperties();
		this.includeAllMetadata = opProperties.getBooleanProperty(SFTPConstants.INCLUDE_ALL);
		this.isAppendEnabled = opProperties.getBooleanProperty(SFTPConstants.APPEND);
		this.createDir = opProperties.getBooleanProperty(SFTPConstants.CREATE_DIR);
		this.uploadRetryFactory = RetryStrategyFactory.createFactory(12);
	}

	private UploadPaths getAndValidateNormalizedPaths(TrackedData input) {
		String enteredFilePath = SFTPUtil.getDocProperty(input, SFTPConstants.PROPERTY_FILENAME);
		if (StringUtil.isBlank(enteredFilePath)) {
			throw new ConnectorException(SFTPConstants.ERROR_MISSING_INPUT_FILENAME);
		}
		SFTPFileMetadata fileMetadata = this.extractRemoteDirAndFileName(input, enteredFilePath);
		String remoteDir = fileMetadata.getDirectory();
		String enteredFileName = fileMetadata.getName();

		return new UploadPaths(remoteDir, enteredFileName);
	}

	private String extractTempFileName(String enteredFileName) {
		if (this.isAppendEnabled) {
			return enteredFileName;
		}
		return enteredFileName + UUID.randomUUID();
	}

	private static boolean mustRenameFile(String tempFileFullPath, String finalFileFullPath) {
		return !ObjectUtil.equals(tempFileFullPath,  finalFileFullPath);
	}

	private SFTPFileMetadata getSFTPFileMetadata(String remoteDir, String fileName) {
		if (!this.includeAllMetadata) {
			return new SimpleSFTPFileMetadata(remoteDir, fileName);
		}
		return this.getSFTPFileMetadataFromRemote(remoteDir, fileName);
	}

	private ExtendedSFTPFileMetadata getSFTPFileMetadataFromRemote(String remoteDir, String fileName) {
		return this.connection.getFileMetadata(remoteDir, fileName);
	}

	private static final class UploadPaths {
		private final String remoteDirFullPath;

		private final String finalFileName;

		UploadPaths(String remoteDirFullPath, String finalFileName) {
			this.remoteDirFullPath = remoteDirFullPath;

			this.finalFileName = finalFileName;

		}

		String getRemoteDirFullPath() {
			return this.remoteDirFullPath;
		}

		String getFinalFileName() {
			return this.finalFileName;
		}

	}

	@Override
	SFTPFileMetadata extractRemoteDirAndFileName(TrackedData input, String filePath) {
		String remoteDir = this.toFullPath(this.connection.getEnteredRemoteDirectory(input));
		RetryableVerifyFileExistsAction fileExistsAction = new RetryableVerifyFileExistsAction(connection, remoteDir);
		fileExistsAction.execute();
		boolean fileExists = fileExistsAction.getFileExists();
		if (createDir && !fileExists) {
			RetryableCreateDirectoryAction createDirectory = new RetryableCreateDirectoryAction(connection, remoteDir);
			createDirectory.execute();
			fileExists = true;
		}

		SFTPFileMetadata fileMetadata = this.pathsHandler.splitIntoDirAndFileName(remoteDir, filePath);
		remoteDir = fileMetadata.getDirectory();
		if (!ObjectUtil.equals( remoteDir,  this.defaultWorkingDirFullPath) && fileExists) {
			RetryableChangeDirectoryAction changeDirAcion = new RetryableChangeDirectoryAction(connection, remoteDir);
			changeDirAcion.execute();
		}
		if (!fileExists) {
			throw new SFTPSdkException(SFTPConstants.ERROR_REMOTE_DIRECTORY_NOT_FOUND);
		}
		return fileMetadata;
	}

	@Override
	void processInput(ObjectData input) {

		String finalFileName;
		String remoteDir;
		RetryableUploadFileAction uploadAction = null;
		try {
			UploadPaths uploadPaths = this.getAndValidateNormalizedPaths((TrackedData) input);
			finalFileName = uploadPaths.getFinalFileName();
			remoteDir = uploadPaths.getRemoteDirFullPath();
			String tempFileName = extractTempFileName(finalFileName);
			String tempFileFullPath = this.pathsHandler.joinPaths(remoteDir, tempFileName);
			long appendOffset = 0L;
			if (isAppendEnabled) {
				RetryableFindSizeOnRemote findSizeAction = new RetryableFindSizeOnRemote(connection, remoteDir,
						finalFileName);
				findSizeAction.execute();
				appendOffset = findSizeAction.getFileSize();
			}
			uploadAction = new RetryableUploadFileAction(this.connection, remoteDir, this.uploadRetryFactory,
					tempFileFullPath, input, appendOffset);
			uploadAction.execute();
			String finalFileFullPath = this.pathsHandler.joinPaths(remoteDir, finalFileName);
			if (UpsertHandler.mustRenameFile(tempFileFullPath, finalFileFullPath)) {
				if (!isAppendEnabled) {
					RetryableVerifyFileExistsAction fileExistsAction = new RetryableVerifyFileExistsAction(connection,
							finalFileFullPath);
					fileExistsAction.execute();
					if (fileExistsAction.getFileExists()) {
						RetryableDeleteFileAtPathAction retrydeleteAtPath = new RetryableDeleteFileAtPathAction(
								this.connection, finalFileFullPath);
						retrydeleteAtPath.execute();
					}
				}
				RetryableRenameFileAction retryRenameFile = new RetryableRenameFileAction(connection, tempFileFullPath,
						finalFileFullPath);
				retryRenameFile.execute();

			}
			SFTPFileMetadata fileMetadata = this.getSFTPFileMetadata(remoteDir, finalFileName);
			operationResponse.addResult((TrackedData) input, OperationStatus.SUCCESS, "0", SFTPConstants.FILE_CREATED,
					fileMetadata.toJsonPayload());
		} catch (Exception ex) {
			input.getLogger().info(ex.getMessage());
			throw ex;
		} finally {
			if (uploadAction != null) {
				uploadAction.close();
			}
		}
	}
}
