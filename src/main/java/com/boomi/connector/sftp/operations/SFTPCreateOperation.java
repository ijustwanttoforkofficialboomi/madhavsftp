/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.operations;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.handlers.UploadHandler;
import com.boomi.connector.util.BaseUpdateOperation;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPCreateOperation extends BaseUpdateOperation {

	public SFTPCreateOperation(SFTPConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		try {
			this.getConnection().openConnection();
			UploadHandler handler = new UploadHandler(this.getConnection(), response);
			handler.processMultiInput(request);
		} catch (Exception e) {
			ResponseUtil.addExceptionFailures(response, (Iterable) request, e);
		} finally {
			this.getConnection().closeConnection();
		}

	}

	@Override
	public SFTPConnection getConnection() {
		return (SFTPConnection) super.getConnection();
	}
}
