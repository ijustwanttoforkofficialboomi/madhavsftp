/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.operations;

import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.SFTPCustomType;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.results.ErrorResult;
import com.boomi.connector.sftp.results.MultiResult;
import com.boomi.connector.util.BaseConnection;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.util.LogUtil;
import java.util.logging.Logger;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPQueryOperation extends BaseQueryOperation {
	public SFTPQueryOperation(SFTPConnection conn) {
		super((BaseConnection) conn);
	}

	protected void executeQuery(QueryRequest queryRequest, OperationResponse operationResponse) {
		SFTPConnection conn = this.getConnection();
		FilterData input = queryRequest.getFilter();
		MultiResult multiResult = new MultiResult(operationResponse, input);
		try {
			conn.openConnection();
			SFTPCustomType type = SFTPCustomType.valueOf(this.getContext().getCustomOperationType());
			switch (type) {
			case LIST: {
				conn.listDirectory(multiResult);
				return;
			}
			case QUERY: {
				conn.queryDirectory(multiResult);
				return;
			}
			default: {
				throw new IllegalArgumentException(SFTPConstants.INVALID_QUERY_TYPE + type);
			}
			}
		} catch (Exception e) {
			LogUtil.severe((Logger) input.getLogger(), SFTPConstants.ERROR_SEARCHING, e);
			OperationStatus status = multiResult.isEmpty() ? OperationStatus.FAILURE
					: OperationStatus.APPLICATION_ERROR;
			multiResult.addPartialResult(new ErrorResult(status, e));

		} finally {
			multiResult.finish();
			conn.closeConnection();
		}
	}

	@Override
	public SFTPConnection getConnection() {
		return (SFTPConnection) super.getConnection();
	}

}
