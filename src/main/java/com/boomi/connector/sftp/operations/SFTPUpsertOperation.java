/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.operations;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.handlers.UpsertHandler;
import com.boomi.connector.util.BaseUpdateOperation;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPUpsertOperation extends BaseUpdateOperation {

	public SFTPUpsertOperation(SFTPConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		try {
			this.getConnection().openConnection();
			UpsertHandler handler = new UpsertHandler(this.getConnection(), response);
			handler.processMultiInput(request);
		} catch (Exception e) {
			ResponseUtil.addExceptionFailures((OperationResponse) response, (Iterable) request, (Throwable) e);
		} finally {
			this.getConnection().closeConnection();
		}
	}

	@Override
    public SFTPConnection getConnection() {
        return (SFTPConnection) super.getConnection();
    }
}
