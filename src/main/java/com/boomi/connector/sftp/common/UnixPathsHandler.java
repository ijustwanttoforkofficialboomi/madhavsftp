/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.common;

import com.boomi.util.StringUtil;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class UnixPathsHandler implements PathsHandler {
	private static final String FILE_PATH_SEPARATOR = "/";

	@Override
	public SFTPFileMetadata splitIntoDirAndFileName(String path) {
		String fileName;
		String dirPath;
		if (path == null) {
			return null;
		}
		int lastSlashPosition = (path = UnixPathsHandler.removeExtraTrailingSeparators(path))
				.lastIndexOf(FILE_PATH_SEPARATOR);
		if (0 < lastSlashPosition && lastSlashPosition < path.length()) {
			dirPath = path.substring(0, lastSlashPosition);
			fileName = path.substring(lastSlashPosition + 1);
		} else if (lastSlashPosition == 0) {
			dirPath = FILE_PATH_SEPARATOR;
			fileName = path.substring(1);
		} else {
			dirPath = "";
			fileName = path;
		}
		return new SimpleSFTPFileMetadata(dirPath, fileName);
	}

	@Override
	public String resolvePaths(String parentPath, String childPath) {
		if (StringUtil.isBlank((String) childPath)) {
			return parentPath;
		}
		if (this.isFullPath(childPath)) {
			return childPath;
		}
		return this.joinPaths(parentPath, childPath);
	}

	@Override
	public SFTPFileMetadata splitIntoDirAndFileName(String parentPath, String childPath) {
		return this.splitIntoDirAndFileName(this.resolvePaths(parentPath, childPath));
	}

	@Override
	public String joinPaths(String parentPath, String childRelativePath) {
		if (StringUtil.isBlank((String) parentPath)) {
			return childRelativePath;
		}
		if (StringUtil.isBlank((String) childRelativePath)) {
			return parentPath;
		}
		return parentPath + (!parentPath.endsWith(FILE_PATH_SEPARATOR) ? FILE_PATH_SEPARATOR : "") + childRelativePath;
	}

	@Override
	public boolean isFullPath(String path) {
		return StringUtil.startsWith((String) path, (String) FILE_PATH_SEPARATOR);
	}

	private static String removeExtraTrailingSeparators(String path) {
		while (StringUtil.endsWith((String) path, (String) FILE_PATH_SEPARATOR) && path.length() > 1) {
			path = path.substring(0, path.length() - 1);
		}
		return path;
	}
}
