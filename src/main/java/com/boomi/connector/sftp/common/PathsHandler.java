/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */

package com.boomi.connector.sftp.common;

 /**
  * @author Omesh Deoli
  *
  * ${tags}
  */
public interface PathsHandler {
	public SFTPFileMetadata splitIntoDirAndFileName(String var1);

	public String resolvePaths(String var1, String var2);

	public SFTPFileMetadata splitIntoDirAndFileName(String var1, String var2);

	public String joinPaths(String var1, String var2);

	public boolean isFullPath(String var1);
}
