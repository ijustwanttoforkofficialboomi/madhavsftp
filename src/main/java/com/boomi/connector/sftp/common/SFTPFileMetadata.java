/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.common;

 /**
  * @author Omesh Deoli
  *
  * ${tags}
  */
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PayloadMetadataFactory;

public interface SFTPFileMetadata {
	public String getDirectory();

	public String getName();

	public PayloadMetadata toPayloadMetadata(PayloadMetadataFactory var1);

	public Payload toJsonPayload();
}
