/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */

package com.boomi.connector.sftp.common;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.sftp.constants.SFTPConstants;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class PropertiesUtil {

	private final BrowseContext context;

	public PropertiesUtil(BrowseContext context) {
		super();
		this.context = context;
	}

	public String getHostname() {
		return context.getConnectionProperties().getProperty(SFTPConstants.PROPERTY_HOST, "");
	}

	public String getUsername() {
		return context.getConnectionProperties().getProperty(SFTPConstants.PROPERTY_USERNAME, "");
	}

	public String getPassword() {
		return context.getConnectionProperties().getProperty(SFTPConstants.PROPERTY_PASSWORD, "");
	}

	public int getPort() {
		return context.getConnectionProperties().getLongProperty(SFTPConstants.PROPERTY_PORT, 22l).intValue();
	}

	public String getRemoteDirectory() {
		return context.getConnectionProperties().getProperty(SFTPConstants.PROPERTY_REMOTE_DIRECTORY);
	}

	public String getAuthType() {
		return context.getConnectionProperties().getProperty(SFTPConstants.AUTHORIZATION_TYPE);
	}

	public String getFileName() {
		return context.getOperationProperties().getProperty(SFTPConstants.PROPERTY_FILENAME);
	}

	public String getActionIfFileExists() {
		return context.getOperationProperties().getProperty(SFTPConstants.OPERATION_PROP_ACTION_IF_FILE_EXISTS);
	}

	public String getpassphrase() {
		return context.getConnectionProperties().getProperty(SFTPConstants.KEY_PSWRD);
	}

	public String getprvtkeyPath() {
		return context.getConnectionProperties().getProperty(SFTPConstants.KEY_PATH);
	}

	public String getTempExtension() {
		return context.getOperationProperties().getProperty(SFTPConstants.PROPERTY_TEMP_EXTENSION);
	}

	public String getStagingDirectory() {
		return context.getOperationProperties().getProperty(SFTPConstants.PROPERTY_STAGING_DIRECTORY);
	}

	public boolean isDeleteAfterReadEnabled() {

		return context.getOperationProperties().getBooleanProperty(SFTPConstants.PROPERTY_DELETE_AFTER);
	}

	public String getKnownHostEntry() {

		return context.getConnectionProperties().getProperty(SFTPConstants.HOST_ENTRY);
	}

	public boolean isMaxExchangeEnabled() {

		return context.getConnectionProperties().getBooleanProperty(SFTPConstants.IS_MAX_EXCHANGE);
	}

	public String getPrivateKeyContent() {
		return context.getConnectionProperties().getProperty(
				SFTPConstants.PRIVATE_KEY_CONTENT);
	}

	public String getPublicKeyContent() {
		return context.getConnectionProperties().getProperty(SFTPConstants.PUBLIC_KEY_CONTENT);
	}
	
	public String getKeyPairName() {
		return context.getConnectionProperties().getProperty(SFTPConstants.KEY_PAIR_NAME);
	}
	
	public boolean isUseKeyContentEnabled() {
		return context.getConnectionProperties().getBooleanProperty(SFTPConstants.USE_KEY_CONTENT);
	}
}
