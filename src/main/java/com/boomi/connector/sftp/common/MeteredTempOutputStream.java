/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.common;

import com.boomi.util.TempOutputStream;
import java.io.IOException;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class MeteredTempOutputStream
extends TempOutputStream {
    private long count;

    public long getCount() {
        return this.count;
    }

    public void write(int b) throws IOException {
        super.write(b);
        ++this.count;
    }

    public void write(byte[] b, int off, int len) throws IOException {
        super.write(b, off, len);
        this.count += (long)len;
    }
}

