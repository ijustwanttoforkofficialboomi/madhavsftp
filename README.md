For forking and pull requests. Ensure you are logged in with your Bitbucket account. Fork repository via the sitemap: https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html
If fork is complete. Please submit a pull request. We'd love to review your code and consider it into the mainline

---

# Building this connector project #
To build this java - maven connector project, you will need to download and unpack the latest (or recent) version of [Maven](https://maven.apache.org/download.cgi) and put the mvn command on your path. 
Then, you will need to install a Java 1.7 preferably, as most of the connectors are designed to work with minimum JDK 1.7 (or higher) JDK (not JRE!), and make sure you can run java from the command line. 
Now you can run `mvn clean` and then `mvn install`. Maven will compile your project, and put the results in the target directory.

---

# SFTP connector #
SFTP connector allows you to transfer data from/to an SFTP server using Java Secure Channel (Jsch). Unlike the standard FTP connector, the SFTP connector encrypts both data and commands to ensure that no sensitive data is exposed when transmitted over a network. Through this connector, you can use a Dell Boomi Integration process to GET and CREATE, the data in any format.

## Connector configuration ##
To configure the connector to communicate with SFTP, please set up two components:

* SFTP connection — The connection represents a specific SFTP enabled server.

* SFTP operation — The operation represents an action against a specific record type. You will create one or more operations, one for each type of interaction required by your integration scenario.

This design provides reusable components containing connection settings and operation settings. After building your connection and operation, set up your connector within a process. When the process is defined properly, Boomi Integration can map to and from virtually any system using the SFTP connector to communicate with the SFTP server.

## Prerequisites ##

* Set the appropriate Java environment variables on your system, such as JAVA_HOME, CLASSPATH.
* SFTP users with the necessary authorization to access remote directories.
* Username and password with the necessary credentials for Dell Boomi.
* Deploy Atom on your local machine or host it with Boomi Integration (Cloud atom).
* Hostname and port number of the SFTP server.
* Any of below authentication type as below
     * Username and Password
     * Using Public Key
* For authentication type “Using Public Key”, Key File Path or keys content (Public and Private) is needed.
* Ensure the SFTP enabled server is accessible from Boomi Integration.
* To test the process in your own cloud atom, follow below steps
	* We need to grant permission in the following files for test connection and process running.
	
	Step 1: (If SFTP server IP is not open to the internet)
	
		/bin/procrunner-HIGH.policy
		/bin/procbrowser-HIGH.policy
		
	* We need to add following line in the above files in Custom permission java.net.SocketPermission "<IP>:<PORT>", "connect,resolve";
		The following link can be used for more details
		
		[https://community.boomi.com/s/article/couldnotcreatesocketcausedbyaccessdeniedjavanetsocketpermission](https://community.boomi.com/s/article/couldnotcreatesocketcausedbyaccessdeniedjavanetsocketpermission)
		
	Step 2: (Instead of giving file path, text field introduced in 1.1 where we can copy-paste the id_rsa content and no need to modify anything on the Policy)
			We need to place the id_rsa private key file in any of the locations where ATOM can access, or we need to grant permission to access the id_rsa key file where located.

	 * This applies to any local network resource. This will grant access to any process running in this cloud (not account specific).

>_**Note:**_  The above steps are to be followed only if the SFTP server is not available/open to the network.

## Supported editions ##
The connector supports

* Any version of SFTP enabled server. 
* SFTP API version of Java Secure Channel (Jsch) library is 0.1.55

## Tracked properties ##
This connector has no tracked properties.

See the topic [Adding tracked fields to a connector operation](https://help.boomi.com/csh?topicname=t-atm-Adding_tracked_fields_to_a_connector_operation_f71821dd-95ee-4ebd-bfc9-3333262f56f6) to learn how to add a custom tracked field.

## Additional resources ##
https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol

# SFTP connection #
The SFTP connection represents a single account including login credentials. If you have multiple systems, you need a separate connection for each.

## Connection Tab ##

### Name ###

### Description ###

### Authentication Type ###
Drop down from which an authentication type can be selected, simple Username and Password or Using Public key authentication. The connector will ignore other field values that are not required for the selected authentication type.

### Remote Directory ###
File directory path on the SFTP server where the connector read/write files from/to.
> _**Note:**_ If not provided, the connector uses the default working directory configured as the base folder on the SFTP server. We can override this directory path using the Remote Directory input document property.

### Host ###
A domain name or IP address of the SFTP host.

### Username ###
Username for SFTP Server. This field is mandatory if the ‘Authentication Type’ is Username and Password.

### Password ###
Password for SFTP user. This field is mandatory if the ‘Authentication Type’ is Username and Password.

### Port ###
The command port on which the SFTP server listens for incoming connections from an SFTP client. The default port is 22.

### Client SSH Key File Path ###
The file system path that specifies the location of the client key file in the server on which atom is running.
> _**Note:**_ This field is mandatory if ‘Authentication Type’ is Using Public Key.

### Key File Password ###
Password for the key file. This field is mandatory if ‘Authentication Type’ is Using Public Key and if the key file is password protected.

## Test Connection ##
You can test your connection settings before you use the connection in a process or even before you save the connection. Test Connection ensures that the connection settings that you specify are correct, valid, and can be accessed. If the test connection is successful, you can save the connection. Otherwise, review and correct any incorrect settings, then test again.

# SFTP operation #
The SFTP operations define how to interact with your SFTP server and represent a specific action (Create and Get) to be performed against a specific file.
Create a separate operation component for each action/object combination that your integration requires.

The SFTP operations supports following actions:

* Inbound: Get
* Outbound: Create

## Options Tab ##
Select a connector action and then use the Import Wizard to select the object with which you want to integrate. When you configure an action (Get and Create), the following fields appear on the Options tab.

## GET ##
Get is an inbound action for which you provide the details of the record which you want to retrieve from the SFTP Server. When using Get to download a file from the server, the file name must be specified as the parameter. The remote directory in which the file is located can optionally be given as document property or connection property. If the remote directory is not provided, the connector uses the default working directory configured as the base folder on the SFTP server.

> _**Note:**_ The File Name should be set as a parameter for successful operation. If not specified, the connector will throw an error.

### Name ###

### Description ###

### Connector Action ###
Determines the type of operation the connector is performing related to Inbound or Outbound, specify the proper connector action. Depending on how you create the operation component, the action type is either configurable or non-configurable from the drop-down list. 

### Object ###
Defines the object with which you want to integrate, and which was selected in the Import Wizard. The Object for Get operation is “File”.

### Response Profile ###
The Response profile will always be Unstructured.

### Tracking Direction ###
This read-only setting shows you the tracking direction (either Input Documents or Output Documents) for the current operation. The default value for Get operation is Output Documents. This setting is determined by the operation configuration in the connector descriptor and affects which document appears in Process Reporting. See the @trackedDocument attribute in the Connector descriptor files topic for more information

### Return Application Error Responses ###
This setting controls whether a server error prevents an operation from completing

* If cleared, the process aborts and reports the error on the Process Reporting page.
* If Selected, failed operations will not be reported in Manage, allowing you to act on them in your process.

### Delete files after reading ###
This setting controls whether a file to be deleted after successful read into the process to avoid duplication of records.

* If cleared, the files will not be deleted after successful read into the process.
* If Selected, the files will be deleted after successful read into the process.

> _**Note:**_ If the file cannot be deleted due to insufficient access rights, a warning message is logged. Dell Boomi recommends selecting this option when executing in production mode to prevent data from being processed twice.

### Fail if unable to delete ###
This setting controls whether a process should be failed after unsuccessful file deletion after a successful read into the process.

* If cleared, the process will continue even after the unsuccessful deletion of files after successful read into the process.
* If Selected, the process will be failed with an error, if an attempt to delete files after reading them into the process is unsuccessful due to insufficient access rights.

## Create ##
Create is an outbound action that you can Upload and add a new file to the server. The request profile contains the properties for a single object. Upon successful record creation, the operation returns a response containing the fully populated record. The file name must be provided using a File Name document property. If the file name is not set, or if the file name already exists, the connector uses the “Action if file exists” setting to determine the next step. 

If the Create operation is successful, the file is uploaded and added to the server and the connector returns a JSON document with information about the file. The file information includes the name of the file, the ID, the user who created the file, when the file was last modified, and other information. Although one SFTP command per file needs to be executed, create will have some kind of batch support since the same SFTP session can be shared for creating more than one file.

Import is required to get uploaded file metadata and successful complete operation.

### Name ###

### Description ###

### Connector Action ###
Determines the type of operation the connector is performing related to inbound, specify the proper connector action. Depending on how you create the operation component, the action type is either configurable or non-configurable from the drop-down list. 

### Object ###
Defines the object with which you want to integrate, and which was selected in the Import Wizard. The Object for Get operation is “File”.

### Request Profile ###
The request profile highlights the format (Structured and Unstructured) in which the File is to be processed. By default, the format will be Unstructured.

### Response Profile ###
The response profile highlights the format (Structured and Unstructured) in which the response is to be processed. 

### Tracking Direction ###
This read-only setting shows you the tracking direction (either Input Documents or Output Documents) for the current operation. The default value for Create operation is Input Documents. This setting is determined by the operation configuration in the connector descriptor and affects which document appears in Process Reporting. See the @trackedDocument attribute in the [Connector descriptor files](https://help.boomi.com/bundle/connectors/page/c-atm-Connector_descriptor_files_3ad80d9a-217c-4fe4-b347-e885d156e0dc.html) topic for more information.
### Return Application Error Responses ###
This setting controls whether an application error prevents an operation from completing:

* If cleared, the process aborts and reports the error on the Process Reporting page.
* If selected, processing continues and passes the error response to the next component to be processed as the connection output.

### Action if File Exists ###
Specify how the operation behaves if the file name already exists in the Remote Directory on the SFTP server. This has four options as below

* Select "Create unique name" to append a number to the file name by the connector to make it unique. 
* Select "Overwrite" to replace the existing content with the new one.  
* Select "Append" to concatenate the new content at the end of the existing one.  
* Select "Generate error" to generate an error.

### Staging Directory (Optional) ###
Option to enter the temporary directory (absolute or relative directory path) to put a file, before being moved to the Remote Directory. 

* If specified, the connector uses the default working directory configured as the base folder on the SFTP server
* If not specified, the file will be created directly in Remote Directory.

### Temporary Extension (Optional) ###
A temporary extension (for example, .tmp or tmp) to append to the file name until the file is created in the Remote Directory. To avoid overwriting existing files, the connector guarantees that the temporary file name is unique when creating files.
After selecting the connector action and using the Import Wizard, you can add query filters

## Archiving tab ##
See the [Connector operation’s Archiving tab](https://help.boomi.com/bundle/connectors/page/r-atm-Connector_operations_Archiving_tab_061fbf70-1034-4bf3-b795-e952f9338dbe.html) for more information.

## Tracking tab ##
See the [Connector operation’s Tracking tab](https://help.boomi.com/bundle/connectors/page/r-atm-Connector_operations_Tracking_tab_8a03f547-738a-448c-bb0f-594bad806cfe.html) for more information.

## Caching tab ##
See the [Connector operation’s Caching tab](https://help.boomi.com/bundle/connectors/page/r-atm-Connector_operations_Caching_tab_f46b49d6-25bc-4337-ade1-9c67817b8d74.html) for more information.

